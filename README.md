# snowflakes

This makes snowflakes fall on any application you put it in.

To use in your Vue.js projects, simply `npm install @chris-sorrells/snowflakes` and then add `<snowflake-generator />` to anything that needs some holiday cheer.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Run your unit tests

```
npm run test:unit
```
