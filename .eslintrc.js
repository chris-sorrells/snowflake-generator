module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/recommended",
    "plugin:prettier/recommended",
    "@vue/prettier"
  ],
  rules: {
    "no-console": "error",
    "no-debugger": "error",
    "vue/html-indent": ["error", 4]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
